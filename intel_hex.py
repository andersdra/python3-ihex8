#!/usr/bin/python3
import subprocess
'''
# Author: Anders Draagen (andersdra@gmail.com)

* edit Intel HEX files with 16 bit addressing (IHEX8)

# quick info about the Intel HEX format
:10246200464C5549442050524F46494C4500464C33
|||||||||||                              CC->Checksum
|||||||||DD->Data
|||||||TT->Record Type
|||AAAA->Address
|LL->Record Length
:->Colon

00 - data record
01 - end-of-file record
# only first 2 data types are used for 16bit addressing
02 - extended segment address record
04 - extended linear address record
05 - start linear address record (MDK-ARM only)

# Checksum calculation
The checksum is calculated by summing the values of all hexadecimal digit pairs \
in the record modulo 256 and taking the two's complement.

# GENERAL: INTEL HEX FILE FORMAT
http://www.keil.com/support/docs/1584/

# Intel HEX
https://en.wikipedia.org/wiki/Intel_HEX
'''

def ihex8_parse(ihex_input):
    ihex = []
    tmp_var = ""
    tmp_list = []
    count = 0
    for char in ihex_input:
        char = chr(char)
        if char == ':' or char == '\n':
            tmp_list.append(char)
            if char == '\n':
                ihex.append(tmp_list)
                tmp_list = []
        else:
            try:
                tmp_var += char
            except TypeError:
                tmp_var += str(char)
            count += 1
            if count == 2:
                tmp_list.append(tmp_var)
                count = 0
                tmp_var = ""
    return ihex


# load hex file to a list of lists[line][char]
def ihex8_load_file(filename):
    file = open(filename, 'r')
    ihex = bytearray(file.read(), 'utf-8')
    file.close()
    ihex = ihex8_parse(ihex)
    return ihex

# value = byte. will be truncated
def ihex8_edit_byte(hex_input, address, value):
    hex_list_copy = list(hex_input)
    line_char = ihex8_calc_addr(hex_list_copy, address)
    line = line_char[0]
    char = line_char[1]
    data = []
    datatype = hex_list_copy[line][4] # byte 4 = Record Type
    if datatype == '00': # data record
        for count, a in enumerate(hex_list_copy[line]):
            try:
                if count == char: # replace byte
                    data.append(value & 0xff)
                else:
                    data.append(int(a, 16)) # put bytes back
            except ValueError: # ':' and '\n'
                data.append(a)
    elif datatype == '01': # end of file
        return
    calculated_checksum = ((~ sum(data[1:-2]) + 1) & 0xff)
    data[-2] = calculated_checksum # remove checksum and newline
    tmpdata = []
    for a in data:
        try:
            tmpdata.append(format(a, 'X').zfill(2)) # uppercase ascii hex 2 chars wide
        except ValueError:
            tmpdata.append(a)
    hex_list_copy[line] = tmpdata
    return hex_list_copy

# hex_input = ihex8_load_file(filename)
def ihex8_check_checksums(hex_input):
    errors = 0
    total = 0
    for line_count, line in enumerate(hex_input):
        for char in line[1:-1]: # slice away ':' and '\n'
            total += int(char, 16)
        chksum = ((~ total + 1) & 0xff)
        if chksum != 0:
            print("Wrong checksum at line {}".format(line_count))
            errors += 1
            return 1 # error
        if errors and line == hex_input[-1]:
            print("{} lines with wrong checksum".format(errors))
        total = 0
    return 0 # everything went well

# check checksums of intel hex file
def ihex8_check_checksums_file(filename):
    tmp_hexfile = ihex8_load_file(filename)
    ihex8_check_checksums(tmp_hexfile)


# get row and column numbers[line][char]
def ihex8_calc_addr(hex_input, address):
    num_data_bytes = int(hex_input[0][1], 16) # extract number of bytes in record at first line
    row = int(address/num_data_bytes)
    col = (address % num_data_bytes) + 5 # add 5 because of start, length, address and data-type bytes
    return int(row), int(col)

# add 0-65534 somewhere
# no error checking if wrong numbers are input
def ihex8_edit_uint16(hex_input, address, value):
    ihex = list(hex_input)
    MSB = (value >> 8) & 0xff
    LSB = value & 0xff
    ihex = ihex8_edit_byte(ihex, address, MSB)
    ihex = ihex8_edit_byte(ihex, address+1, LSB)
    return ihex

# save modified hex, overwrites if exists
def ihex_save_file(hex_input, filename):
    file = open(filename, 'w')
    for line in hex_input:
        for char in line:
            file.write(char)
    file.close()

# dump specified AVR memory
def avrdude_dump(memory_to_dump, part, programmer):
    avrdude_string = "/usr/bin/avrdude" + " -p " + part + " -c " + programmer + " -B 1" + " -U "  + memory_to_dump + ":r:-:i" + " -C avrdude.conf"
    try:
        avrdump = subprocess.check_output(avrdude_string.split(' '), stderr=subprocess.DEVNULL, shell=False)
        return avrdump
    except subprocess.CalledProcessError as avrexec:
        print("avrdude error")

# dump AVR memory and parse
def avrdude_dump_list(memory_to_dump, part, programmer):
    tmp_list = []
    avrdump = avrdude_dump(memory_to_dump, part, programmer)
    try:
        for char in avrdump:
            tmp_list.append(chr(char))
    except TypeError:
        print("AVR/PROGRAMMER NOT CONNECTED??")
        quit()
    ihex = ihex8_parse(tmp_list)
    return ihex


tmp = ihex8_load_file("./hex_examples/ihex32.eep")
#tmp = hex_load_file("shortdump.hexfile")
uint_edit = ihex8_edit_byte(tmp, 0, 105)
uint_edit = ihex8_edit_byte(uint_edit, 2, 150)
uint_edit = ihex8_edit_uint16(uint_edit, 31, 1000)
ihex_save_file(uint_edit, "./hex_examples/blabla.eep")

tmp_flash = ihex8_load_file("./hex_examples/eeprom_read.hex") # load simple arduino sketch
flash_edit = ihex8_edit_byte(tmp_flash, 0, 0x69) # put 0x69 at address 0
flash_edit[1][-2] = '96' # change to wrong checksum
ihex8_check_checksums(flash_edit) # confirm there is an error
ihex_save_file(flash_edit, "./hex_examples/wrongchk.hex") # save
ihex8_check_checksums_file("./hex_examples/wrongchk.hex") # check checksums of file

#tss = avrdude_dump_list("eeprom", "m328p", "prodasp0") # get eeprom straight from AVR to list of lists
#print("TTT: ", tss)
#tss = ihex8_edit_byte(tss, 0, 105)
#print(tss)
